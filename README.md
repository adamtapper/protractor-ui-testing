# Protractor UI Testing

Protractor is an end-to-end test framework for web-based applications. Protractor runs tests against your application running in a real browser, interacting with it as a user would. This framework also integrates lighthouse for accessibility testing of a site and the protractor-image-comparison tool for visual testing.

## Contents
* [Setting Up Framework](#markdown-header-setting-up-framework)
    * [Using The Demo](#markdown-header-using-the-demo)
* [Running Tests](#markdown-header-running-tests)
* [Reports](#markdown-header-reports)
    * [Protractor Reports](#markdown-header-protractor-reports)
    * [Accessibility Reports](#markdown-header-accessibility-reports)
* [Framework Utility Classes](#markdown-header-framework-utility-classes)
    * [Accessibility Utility](#markdown-header-accessibility-utility)
    * [Browser Utility](#markdown-header-browser-utility)
    * [Command Line Utility](#markdown-header-command-line-utility)
    * [Date Time Utility](#markdown-header-date-time-utility)
    * [Jasmine Config Utility](#markdown-header-jasmine-config-utility)
    * [Report Utility](#markdown-header-report-utility)
    * [Mobile Utility](#markdown-header-mobile-utility)
* [Protractor Knowledge](#markdown-header-protractor-knowledge)
    * [Configuration File](#markdown-header-configuration-file)
    * [Protractor Api](#markdown-header-protractor-api)
    * [Using async/await](#markdown-header-using-async/await)
    * [Supported Browsers](#markdown-header-supported-browsers)
* [Appium](#markdown-header-appium-optional)
    * [Setting up Protractor-Appium](#markdown-header-setting-up-protractor-appium)
        * [iOS Specific Config](#markdown-header-ios-specific-config)
        * [Android Specific Config:](#markdown-header-android-specific-config)
    * [Running Appium Tests](#markdown-header-running-appium-tests)
        * [Running Appium Server Separate](#markdown-header-running-appium-server-separate)
        * [Running Appium Server And Protractor Tests In One Command](#markdown-header-running-appium-server-and-protractor-tests-in-one-command)
* [Appium Knowledge](#markdown-header-appium-knowledge)
    * [Appium API](#markdown-header-appium-api)
    * [Appium Capabilities](#markdown-header-appium-capabilities)
* [Contribution](#markdown-header-contribution)
* [Additional Resources](#markdown-header-additional-resources)

## Setting Up Framework
* A working example of the configuration as well as each test type can be seen here: [Demo](https://bitbucket.org/adamtapper/protractor-ui-testing/src/master/demo)
    * Accessibility tests can only be run in chrome
* Ensure Nodejs is installed
* Download the framework locally and extract the protractor-ui-testing-x.x.x folder to the code base where your tests will live
* Create a jasmine-config.js file in the directory where the tests will live and add the following code at minimum
```javascript
const JasmineConfigUtility = require('protractor-ui-testing/src/jasmine-config-utility');
exports.config = JasmineConfigUtility.getDefaultConfig([PATH_TO_SPEC_FILES], {SUITES}, BASE_URL);
```
* Add the following to the dev dependencies section of the package.json
```
"jasmine": "^3.4.0",
"jasmine-reporters": "^2.3.2",
"lighthouse": "^5.2.0",
"protractor": "^5.4.3",
"protractor-html-reporter-2": "^1.0.4",
"protractor-image-comparison": "^3.9.0"
"protractor-ui-testing": "RELATIVE_PATH/protractor-ui-testing-x.x.x"
```
* Add the following to the scripts section of the package.json
```
"protractor": "protractor RELATIVE_PATH/jasmine-config.js",
"selenium-start": "webdriver-manager start",
"update-driver": "webdriver-manager update",
```
* Run ```npm install```
* Run ```npm run update-driver``` to download webdrivers

### Using The Demo
* ```cd demo``` to enter demo directory
* Run ```npm install```
* Run ```npm run update-driver```
* Run ```npm run protractor -- --browser=CHOSEN_BROWSER```

## Running Tests
* Execute command ```npm run protractor -- --browser=Chosen_Browser(s)```
    * browser is a comma separated list with no spaces in between of the browsers you want to execute in. e.g.: headlessChrome,firefox
    * directConnect is enabled by default but this only supports firefox and chrome. To use safari, etc. also pass the --direct=false flag to the run command
        * If not using direct connect, also execute ```npm run selenium-start``` to start the selenium server that will be used to send commands to the webdriver

## Reports

### Protractor Reports
* Protractor reports are output to a report.html file in a datetime stamped folder in the report directory.
* Sample Report: ![image info](./readme-images/protractor-report.png)

### Accessibility Reports
* Lighthouse reports are output to an accessibility.html file in the report directory.
* Sample Report: ![image info](./readme-images/lighthouse-report.png)

## Framework Utility Classes

### Accessibility Utility
* runLighthouse (url, opts, reportDir, config?): runs lighthouse accessibility test, generates html report from results and returns overall score

### Browser Utility
* getBrowserConfig (browserList): returns an object with the configuration(s) of the browsers that are passed in

### Command Line Utility
* getCommandLineArg (argument): retrieves any command line argument passed in the --ARG format
* getBrowser (): retrieve browser list passed via --browser command line arg
* getDirectConnect (): retrieves when direct connect should be on or off (on by default)
* getEnvironment (): retrieve environment passed via --env command line arg
* isHeadLess(): retrieve isHeadless passed via  --headless command line arg. This argument is used for running mobiles tests headless.
* getDevice(): retrieve mobile device passed via --device command line args (by default for iOS: 'iPhone X' & android:'Pixel 3')
* getPlatformVersion(): retrieve mobile platform version via --platformVersion command line args (by default platformVersion= ```12.4```)

### Date Time Utility
* getDateTime (date): returns date an time in yyyy-mm-dd_hh:mm:ss format
* getReportDateTime (date): returns date an time in yyyy-mm-dd_hh-mm- sss format

### Jasmine Config Utility
* setReportDirectory (reportDirectory): set directory where Protractor reports are output to
* setReportTitle (reportTitle): set title of Protractor reports
* getDesktopConfig (specPaths, suites, baseUrl): retrieve Protractor config object
* getMobileConfig(specPath,suites): retrieve config object that runs for Mobile
* getDefaultConfig(specPaths,suites,baseUrl) : which contains the logical statement that returns the either desktop config object or mobile config object based on the command line arg 'browser'

### Report Utility
* moveDiffImages (): renames diff images from failing visual tests and moves them so that they render properly in the Protractor report
* getReportTitle (): retrieves the report title
* getDashboardReportDirectory (): gets report directory with the datetime stamp

### Mobile Utility
* getMobileConfig(platform,deviceList) : returns object with the configuration(s) of the mobile platforms (ios/android) and  devices that are passed in

## Protractor Knowledge

### Configuration File
* All available elements for the Protractor configuration file on defined on its GitHub page
* [Protractor Config File](https://github.com/angular/protractor/blob/master/lib/config.ts)

### Protractor Api
* Protractor defines all methods that can used to retrieve elements from the page, control the browser and manage the control flow
* [Protractor Api Methods](https://www.protractortest.org/#/api)

### Using async/await
* async/await can be used to ensure that events in the webdriver control flow reach the browser at the appropriate time
* [Protractor async/await Documentation](https://www.protractortest.org/#/async-await)

### Supported Browsers
* [Supported Browsers](https://www.protractortest.org/#/browser-support)

## Appium (optional)

### Setting up Protractor-Appium 
* Complete [Setting Up Framework](#markdown-header-setting-up-framework)
* Add the following to the dev dependencies section of the package.json
```
"appium": "^1.14.2",
"appium-doctor": "^1.11.1",
"npm-run-all": "^4.1.5"
```
* Add the following to the scripts section of the package.json
```
"appium-doctor-android": "appium-doctor --android",
"appium-doctor-ios": "appium-doctor --ios",
"appium-protractor": "npm run protractor -- --browser=$browser"
"start-appium": "appium &",
"ios-fix": "cd node_modules/appium/node_modules/appium-webdriveragent/ && mkdir -p Resources/WebDriverAgent.bundle && ./Scripts/bootstrap.sh"
```
* Run ```npm install```
* Complete [iOS Specific Config](#markdown-header-ios-specific-config)
* Complete [Android Specific Config:](#markdown-header-android-specific-config)

#### iOS Specific Config
* Install carthage ```brew install carthage```
* Install XCODE latest version from the App Store.
* Run ```npm run ios-fix``` in demo project in order to fix the error ```[{"errno":"ECONNREFUSED","code":"ECONNREFUSED","syscall":"connect","address":"127.0.0.1","port":8100}]```
    * Note: This command will create /Resources/WebDriverAgent.bundle directory in node_modules/appium/node_modules/appium-webdriveragent and run the bootstrap.sh from Scripts [http://appium.io/docs/en/advanced-concepts/wda-custom-server/]
* Run ```npm run appium-doctor-ios``` to check the iOS setup status (Make sure all necessary dependencies should be ```✔```)

#### Android Specific Config
* Install Android Studio latest version [here](https://developer.android.com/studio)
* Open Android Studio
* Click on Start a new Android Project
    * Use all default options when configuring project
* Let the project and gradle build process finish
* Navigate to the top menu bar and click Tools > AVD Manager 
    * If the AVD Manager is not an option under tools, ensure you have the Android SDK installed from [here](http://www.androiddocs.com/sdk/installing/index.html)
* Create a Android Virtual Device
* Double click your created device to start it
* To see a list of active devices running
    * Setup the ANDROID_HOME environment variable to the {YOUR_MACHINE_PATH}/Android/sdk/)
    * Run the command ```adb devices```
* Run ```npm run appium-doctor-android``` to check the iOS setup status (Make sure all necessary dependencies should be ```✔```)
    * If you see the error 'No Chromedriver found that can automate Chrome ${version}.'
        * Download the needed chromedriver from [here](https://chromedriver.chromium.org/downloads)
        * Place it in desired folder within project
        * Change appium script in package.json from ```"appium": "appium"``` to ```"appium": "appium --chromedriver-executable PATH_TO_YOUR_DOWNLOADED_CHROMEDRIVER_FOLDER/chromedriver" ```
            * For more details please see this link (http://appium.io/docs/en/writing-running-appium/web/chromedriver/)

### Running Appium Tests
#### Running Appium Server Separate
* Execute command ```npm run appium``` in another terminal to run appium
* Execute command ``` npm run protractor -- --browser=Chosen_Platform```
    * browser here is either ios or android e.g: ios
    * device is a comma separated list with no spaces in between of the devices you want to execute in e.g: 'iPhone 8','iPhone 7'
        * (Note: if you do not pass --device iOS by default device set to 'iPhone X' and Android by default device set to 'Pixel 3')
    * (Note: if you see any issues with iOS simulator please see [iOS Specific Config](#markdown-header-ios-specific-config))    
#### Running Appium Server And Protractor Tests In One Command        
* Instead of running appium and tests in two separate terminals use below command
    ```browser=ios npm-run-all appium-run-background appium-protractor```
    * Above command will start appium in background, if we need kill appium use this command ```lsof -i tcp:4723``` get the PID and ```kill <PID>```

## Appium Knowledge

### Appium API
* Appium all methods that can used to manage mobile control flow.
* [Appium Api Methods](http://appium.io/docs/en/about-appium/api/)

### Appium Capabilities
* [Appium mobile specific configurations (Android & iOS)](http://appium.io/docs/en/drivers/ios-xcuitest/#capabilities)


## Contribution
* In order to contribute to the framework
    * ```cd protractor-ui-testing-1.2.0``` to enter protractor framework directory
    * Run ```npm install```
    * Write class code
    * Write unit tests (```npm test``` to run tests)
    * Run ```npm run lint``` and fix any remaining errors
    * Update framework version in folder name
        * e.g. protractor-ui-testing-1.2.0 -> protractor-ui-testing-1.3.0
        * minor versions (0.0.x) used for bug fixes/minor changes
    * Update demo with example use of code if necessary
    * Update documentation
    * Submit pull request

## Additional Resources
* [Lighthouse](https://www.npmjs.com/package/lighthouse)
* [Protractor-Image-Comparison](https://www.npmjs.com/package/protractor-image-comparison)
* [Appium](http://appium.io/)
