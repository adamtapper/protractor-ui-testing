const suite = describe('Visual Test Demo', function () {
  const spec = it('should have a title comparison', async function () {
    browser.waitForAngularEnabled(false);
    browser.driver.manage().window().setSize(1920, 1080);
    await browser.get('https://www.slalom.com');
    await browser.sleep(15000);
    expect(await browser.imageComparison.checkScreen(`${suite.description + ' ' + spec.description}`.replace(/_/g, ' '))).toEqual(0);
  });
});
