const browser = require('protractor').browser;

const AccessibilityUtility = require('../../protractor-ui-testing-1.2.0/src/accessibility-utility');

describe('Accessibility Test Demo', function () {
  it('should have a high accessibility score', async function () {
    browser.waitForAngularEnabled(false);
    browser.get('https://pokemoncenter.com');
    const opts = {
      onlyCategories: ['accessibility'],
      port: 9222
    };
    await AccessibilityUtility.runLighthouse('https://example.com', opts, process.cwd() + '/reports/').then(async results => {
      expect(results.categories.accessibility.score * 100).toBeGreaterThan(80);
    });
  });
});
