describe('Mobile Test Suite',function(){
  it('should open slalombuild home page',async () => {
    browser.waitForAngularEnabled(false);
    await browser.get('/about');
    const result = browser.getTitle();
    expect(result).toEqual('About building the future | Slalom Build');
  })
})