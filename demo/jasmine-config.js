const JasmineConfigUtility = require('../protractor-ui-testing-1.2.0/src/jasmine-config-utility');
JasmineConfigUtility.setReportDirectory(process.cwd() + '/reports');
JasmineConfigUtility.setReportTitle('Protractor Framework Demo');
exports.config = JasmineConfigUtility.getDefaultConfig([process.cwd() + '/tests/*.js'], {
  mobile:process.cwd()+'/tests/mobile-test.js'
}, 'https://www.slalombuild.com');