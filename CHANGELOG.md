# Changelog
All notable changes to this project will be documented in this file.

## [1.0.0] - 2019-07-29
### Added
- [Accessibility Utility](https://bitbucket.org/adamtapper/protractor-ui-testing/src/master/README.md#markdown-header-accessibility-utility)
- [Browser Utility](https://bitbucket.org/adamtapper/protractor-ui-testing/src/master/README.md#markdown-header-browser-utility)
- [Command Line Utility](https://bitbucket.org/adamtapper/protractor-ui-testing/src/master/README.md#markdown-header-command-line-utility)
- [Date Time Utility](https://bitbucket.org/adamtapper/protractor-ui-testing/src/master/README.md#markdown-header-date-time-utility)
- [Jasmine Config Utility](https://bitbucket.org/adamtapper/protractor-ui-testing/src/master/README.md#markdown-header-jasmine-config-utility)
- [Report Utility](https://bitbucket.org/adamtapper/protractor-ui-testing/src/master/README.md#markdown-header-report-utility)
- [Initial Documentation](https://bitbucket.org/adamtapper/protractor-ui-testing/src/master/README.md)

## [1.1.0] - 2019-10-7
### Added
- [Appium Setup](https://bitbucket.org/adamtapper/protractor-ui-testing/src/master/README.md#markdown-header-appium)
- [Mobile Utility](https://bitbucket.org/adamtapper/protractor-ui-testing/src/master/README.md#markdown-header-mobile-utility)
### Changed
- Use dashes instead of colons in the timestamp of report folder name for Windows compatibility

## [1.2.0] - 2020-03-31
### Changed
- Updated protractor version from 5.4.2 to 5.4.3
- Updated protractor-image-comparison from 3.2.0 to 3.9.0
- Updated appium from 1.15.0 to 1.17.0