const DateTimeUtility = require('../src/date-time-utility');

describe('Date Time Utility', function () {
  it('should return current date time with 0 in front of single digits', function () {
    const date = new Date('2019-05-06T07:08:09');
    expect(DateTimeUtility.getDateTime(date)).toEqual('2019-05-06_07:08:09');
  });

  it('should return current date time when no single digits present', function () {
    const date = new Date('2019-10-11T12:13:14');
    expect(DateTimeUtility.getDateTime(date)).toEqual('2019-10-11_12:13:14');
  });

  it('should return current date time when no single colons present', function () {
    const date = new Date('2019-10-11T12:13:14');
    expect(DateTimeUtility.getReportDateTime(date)).toEqual('2019-10-11_12-13-14');
  });
});
