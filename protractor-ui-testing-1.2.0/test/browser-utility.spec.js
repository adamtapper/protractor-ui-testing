const BrowserUtility = require('../src/browser-utility');

describe('Browser Utility', function () {
  it('should return multiple browser configurations in an array', function () {
    expect(BrowserUtility.getBrowserConfig(['chrome', 'firefox'])).toEqual(
      [{
        browserName: 'chrome',
        useAutomationExtension: false,
        chromeOptions: {
          args: ['--remote-debugging-port=9222']
        }
      },
      {
        browserName: 'firefox'
      }
      ]
    );
  });

  it('should return chrome browser configuration', function () {
    expect(BrowserUtility.getBrowserConfig(['chrome'])).toEqual(
      [{
        browserName: 'chrome',
        useAutomationExtension: false,
        chromeOptions: {
          args: ['--remote-debugging-port=9222']
        }
      }]
    );
  });

  it('should return firefox browser configuration', function () {
    expect(BrowserUtility.getBrowserConfig(['firefox'])).toEqual(
      [{
        browserName: 'firefox'
      }]
    );
  });

  it('should return safari browser configuration', function () {
    expect(BrowserUtility.getBrowserConfig(['safari'])).toEqual(
      [{
        browserName: 'safari'
      }]
    );
  });

  it('should return headless chrome browser configuration', function () {
    expect(BrowserUtility.getBrowserConfig(['headlessChrome'])).toEqual(
      [{
        browserName: 'chrome',
        useAutomationExtension: false,
        chromeOptions: {
          args: ['--headless', '--disable-gpu', '--remote-debugging-port=9222']
        }
      }]
    );
  });

  it('should return headless firefox browser configuration', function () {
    expect(BrowserUtility.getBrowserConfig(['headlessFirefox'])).toEqual(
      [{
        browserName: 'firefox',
        'moz:firefoxOptions': {
          args: ['--headless']
        }
      }]
    );
  });
});
