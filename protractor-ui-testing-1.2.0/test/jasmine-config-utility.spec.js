const jasmineConfigUtility = require('../src/jasmine-config-utility');
describe('UT_BLT_Jasmine Config Utility', function () {
  it('UT_BLT_getDefaultConfig_getConfigObjectForIOSDevices_returnIOSDeviceConfigObject', function () {
    process.argv = ['--browser=ios'];
    expect(jasmineConfigUtility.getDefaultConfig())
      .toEqual({
        onPrepare: jasmine.any(Function),
        onComplete: jasmine.any(Function),
        seleniumAddress: 'http://localhost:4723/wd/hub',
        specs: undefined,
        suites: undefined,
        multiCapabilities: [Object({
          browserName: 'safari',
          platformName: 'iOS',
          platformVersion: '12.4',
          orientation: 'PORTRAIT',
          automationName: 'XCUITest',
          app: '',
          noReset: false,
          fullReset: true,
          isHeadless: false,
          newCommandTimeout: 240,
          deviceName: 'iPhone X'
        })],
        jasmineNodeOpts: Object({
          showColors: true,
          defaultTimeoutInterval: 30000
        }),
        baseUrl: undefined
      });
  });

  it('UT_BLT_getDefaultConfig_getConfigObjectForDesktopDevices_returnDesktopDeviceConfigObject', function () {
    process.argv = ['--browser=chrome'];
    expect(jasmineConfigUtility.getDefaultConfig())
      .toEqual({
        onPrepare: jasmine.any(Function),
        onComplete: jasmine.any(Function),
        plugins: jasmine.any(Array),
        directConnect: true,
        specs: undefined,
        suites: undefined,
        multiCapabilities: [Object({
          browserName: 'chrome',
          useAutomationExtension: false,
          chromeOptions: Object({
            args: ['--remote-debugging-port=9222']
          })
        })],
        seleniumAddress: 'http://localhost:4444/wd/hub',
        baseUrl: undefined,
        jasmineNodeOpts: Object({
          showColors: true,
          defaultTimeoutInterval: 30000
        })
      });
  });

  it('UT_BLT_getDefaultConfig_getConfigObjectForDesktopDevices_shouldNotReturnIosDeviceConfigObject', function () {
    process.argv = ['--browser=chrome'];
    expect(jasmineConfigUtility.getDefaultConfig())
      .not.toEqual({
        onPrepare: jasmine.any(Function),
        onComplete: jasmine.any(Function),
        seleniumAddress: 'http://localhost:4723/wd/hub',
        specs: undefined,
        suites: undefined,
        multiCapabilities: [Object({
          browserName: 'safari',
          platformName: 'iOS',
          platformVersion: '12.4',
          orientation: 'PORTRAIT',
          automationName: 'XCUITest',
          app: '',
          noReset: false,
          fullReset: true,
          isHeadless: false,
          newCommandTimeout: 240,
          deviceName: 'iPhone X'
        })],
        jasmineNodeOpts: Object({
          showColors: true,
          defaultTimeoutInterval: 30000
        })
      });
  });

  it('UT_BLT_getDefaultConfig_getConfigObjectForIosDevices_shouldNotReturnDesktopDeviceConfigObject', function () {
    process.argv = ['--browser=ios'];
    expect(jasmineConfigUtility.getDefaultConfig())
      .not.toEqual({
        onPrepare: jasmine.any(Function),
        onComplete: jasmine.any(Function),
        plugins: jasmine.any(Array),
        directConnect: true,
        specs: undefined,
        suites: undefined,
        multiCapabilities: [Object({
          browserName: 'chrome',
          useAutomationExtension: false,
          chromeOptions: Object({
            args: ['--remote-debugging-port=9222']
          })
        })],
        seleniumAddress: 'http://localhost:4444/wd/hub',
        baseUrl: undefined,
        jasmineNodeOpts: Object({
          showColors: true,
          defaultTimeoutInterval: 30000
        })
      });
  });
});
