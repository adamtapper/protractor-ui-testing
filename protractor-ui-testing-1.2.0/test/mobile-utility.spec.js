const mobileUtility = require('../src/mobile-utility');

describe('UT_BLT_Browser Utility', function () {
  it('UT_BLT_getMobileConfig_passBrowserAndMultiDeviceArguments_returnsAnArrayWithBrowserObjectForMultiDevice', function () {
    expect(mobileUtility.getMobileConfig('ios', ['iPhone 8', 'iPhone 7'])).toEqual(
      [{
        browserName: 'safari',
        platformName: 'iOS',
        deviceName: 'iPhone 8',
        platformVersion: '12.4',
        orientation: 'PORTRAIT',
        automationName: 'XCUITest',
        app: '',
        noReset: false,
        fullReset: true,
        isHeadless: false,
        newCommandTimeout: 240
      }, {
        browserName: 'safari',
        platformName: 'iOS',
        deviceName: 'iPhone 7',
        platformVersion: '12.4',
        orientation: 'PORTRAIT',
        automationName: 'XCUITest',
        app: '',
        noReset: false,
        fullReset: true,
        isHeadless: false,
        newCommandTimeout: 240
      }]
    );
  });

  it('UT_BLT_getMobileConfig_passBrowserAndSingleDeviceArguments_returnsAnArrayWithBrowserObjectForSingleDevice', function () {
    expect(mobileUtility.getMobileConfig('ios', ['iPhone 8'])).toEqual(
      [{
        browserName: 'safari',
        platformName: 'iOS',
        deviceName: 'iPhone 8',
        platformVersion: '12.4',
        orientation: 'PORTRAIT',
        automationName: 'XCUITest',
        app: '',
        noReset: false,
        fullReset: true,
        isHeadless: false,
        newCommandTimeout: 240
      }]
    );
  });

  it('UT_BLT_getMobileConfig_passBrowserAndSingleDeviceArguments_shouldNotReturnDefaultDeviceObject', function () {
    expect(mobileUtility.getMobileConfig('ios', ['iPhone 8'])).not.toEqual(
      [{
        browserName: 'safari',
        platformName: 'iOS',
        deviceName: 'iPhone X',
        platformVersion: '12.4',
        orientation: 'PORTRAIT',
        automationName: 'XCUITest',
        app: '',
        noReset: false,
        fullReset: true,
        isHeadless: false,
        newCommandTimeout: 240
      }]
    );
  });

  it('UT_BLT_getMobileConfig_passAndroidAndSingleDeviceArguments_returnsAnArrayWithAndroidObjectForSingleDevice', function () {
    expect(mobileUtility.getMobileConfig('android', ['Pixel 3'])).toEqual(
      [{
        browserName: 'Chrome',
        platformName: 'Android',
        automationName: 'UiAutomator2',
        deviceName: 'Pixel 3',
        nativeWebScreenshot: true,
        chromedriverArgs: ['--disable-gpu']
      }]
    );
  });
});
