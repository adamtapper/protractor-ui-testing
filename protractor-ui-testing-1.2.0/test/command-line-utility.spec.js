const CommandLineUtility = require('../src/command-line-utility');

describe('Command Line Utility', function () {
  [
    ['--browser=chrome,firefox,safari', ['chrome', 'firefox', 'safari']],
    ['--browser=chrome', ['chrome']],
    ['--browser=android', ['android']],
    ['--browser=ios', ['ios']]
  ].forEach(([commandLineArgument, expectedOutput]) => {
    it(`should output browser list as ${expectedOutput} for command line argument "${commandLineArgument}"`, () => {
      process.argv = [commandLineArgument];
      expect(CommandLineUtility.getBrowser()).toEqual(expectedOutput);
    });
  });

  [
    ['--device=iPhone 7', ['iPhone 7']],
    ['--device=iPhone 8,iPhone 6S', ['iPhone 8', 'iPhone 6S']],
    ['--browser=android', ['Pixel 3']],
    ['--browser=ios', ['iPhone X']]
  ].forEach(([commandLineArgument, expectedOutput]) => {
    it(`should output device as ${expectedOutput} for command line argument "${commandLineArgument}"`, () => {
      process.argv = [commandLineArgument];
      expect(CommandLineUtility.getDevice()).toEqual(expectedOutput);
    });
  });

  [
    ['--platformVersion=13.5', '13.5'],
    ['--platformVersion=', '12.4'],
    ['--browser=android', '12.4']
  ].forEach(([commandLineArgument, expectedOutput]) => {
    it(`should output platform as ${expectedOutput} for command line argument "${commandLineArgument}"`, () => {
      process.argv = [commandLineArgument];
      expect(CommandLineUtility.getPlatformVersion()).toEqual(expectedOutput);
    });
  });

  [
    ['--headless=false', false],
    ['--headless=true', true]
  ].forEach(([commandLineArgument, expectedOutput]) => {
    it(`should output headless option as ${expectedOutput} for command line argument "${commandLineArgument}"`, () => {
      process.argv = [commandLineArgument];
      expect(CommandLineUtility.isHeadLess()).toEqual(expectedOutput);
    });
  });

  [
    ['--direct=false', false],
    ['--direct=true', true]
  ].forEach(([commandLineArgument, expectedOutput]) => {
    it(`should output direct connect option as ${expectedOutput} for command line argument "${commandLineArgument}"`, () => {
      process.argv = [commandLineArgument];
      expect(CommandLineUtility.getDirectConnect()).toEqual(expectedOutput);
    });
  });


  it('should return environment', function () {
    process.argv = ['--env=qa'];
    expect(CommandLineUtility.getEnvironment()).toEqual('qa');
  });
});
