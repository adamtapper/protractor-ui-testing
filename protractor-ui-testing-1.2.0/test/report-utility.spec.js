const ReportUtility = require('../src/report-utility');

describe('Report Utility', function () {
  it('should have a default report directory', function () {
    const reportUtility = new ReportUtility();
    expect(reportUtility.getReportDirectory()).toEqual('./reports');
  });

  it('should allow setting report directory', function () {
    const reportUtility = new ReportUtility();
    const reportDirectory = './repor';

    reportUtility.setReportDirectory(reportDirectory);

    expect(reportUtility.getReportDirectory()).toEqual(reportDirectory);
  });

  it('should have a default report title', function () {
    const reportUtility = new ReportUtility();
    expect(reportUtility.getReportTitle()).toEqual('Protractor Test Execution Report');
  });

  it('should allow setting report title', function () {
    const reportUtility = new ReportUtility();
    const reportTitle = 'Title';

    reportUtility.setReportTitle(reportTitle);

    expect(reportUtility.getReportTitle()).toEqual(reportTitle);
  });
});
