const lighthouse = require('lighthouse');
const fs = require('fs');

class AccessibilityUtility {
  static runLighthouse (url, opts, reportDir, config = { extends: 'lighthouse:default', settings: { output: 'html' } }) {
    return lighthouse(url, opts, config).then(results => {
      fs.writeFileSync(reportDir + 'accessibility.html', results.report, (err) => {
        if (err) { console.log(err); } else { console.log('Successfully Written to File.'); }
      });
      return results.lhr;
    });
  }
}

module.exports = AccessibilityUtility;
