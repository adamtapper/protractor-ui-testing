const CommandLineUtility = require('./command-line-utility');
const BrowserUtility = require('./browser-utility');
const ReportUtility = require('./report-utility');
const reportUtility = new ReportUtility();
const MobileUtility = require('./mobile-utility');

class JasmineConfigUtility {
  static setReportDirectory (reportDirectory) {
    reportUtility.setReportDirectory(reportDirectory);
  }

  static setReportTitle (reportTitle) {
    reportUtility.setReportTitle(reportTitle);
  }

  static getDefaultConfig (specPaths, suites, baseUrl) {
    return (CommandLineUtility.getBrowser()[0] === 'ios' || CommandLineUtility.getBrowser()[0] === 'android') ? this.getMobileConfig(specPaths, suites, baseUrl) : this.getDesktopConfig(specPaths, suites, baseUrl);
  }

  static getDesktopConfig (specPaths, suites, baseUrl) {
    return {
      onPrepare: function () {
        reportUtility.addReporter(jasmine);
      },
      onComplete: function () {
        reportUtility.moveDiffImages();
        reportUtility.createReport();
      },
      plugins: [{
        package: 'protractor-image-comparison',
        options: {
          baselineFolder: reportUtility.getReportDirectory() + '/baseline/',
          screenshotPath: reportUtility.getDashboardReportDirectory(),
          savePerInstance: true,
          autoSaveBaseline: true,
          blockOutToolBar: true,
          clearRuntimeFolder: true,
          formatImageName: '{browserName}-{tag}'
        }
      }],
      directConnect: CommandLineUtility.getDirectConnect(),
      specs: specPaths,
      suites: suites,
      multiCapabilities: BrowserUtility.getBrowserConfig(CommandLineUtility.getBrowser()),
      seleniumAddress: 'http://localhost:4444/wd/hub',
      baseUrl: baseUrl,

      jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000
      }
    };
  };

  static getMobileConfig (specPaths, suites, baseUrl) {
    return {
      onPrepare: function () {
        reportUtility.addReporter(jasmine);
      },
      onComplete: function () {
        reportUtility.moveDiffImages();
        reportUtility.createReport();
      },
      seleniumAddress: 'http://localhost:4723/wd/hub',
      specs: specPaths,
      suites: suites,
      multiCapabilities: MobileUtility.getMobileConfig(CommandLineUtility.getBrowser()[0], CommandLineUtility.getDevice()),
      jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000
      },
      baseUrl: baseUrl
    };
  }
}

module.exports = JasmineConfigUtility;
