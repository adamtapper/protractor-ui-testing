const process = require('process');

class CommandLineUtility {
  static getCommandLineArg (argument) {
    let browser = '';
    process.argv.forEach(arg => {
      if (arg.split('=')[0] === `--${argument}`) {
        browser = arg.split('=')[1];
      }
    });
    return browser;
  }

  static getBrowser () {
    return this.getCommandLineArg('browser').split(',');
  }

  static getDirectConnect () {
    let direct = true;
    if (this.getCommandLineArg('direct') !== '') {
      direct = this.getCommandLineArg('direct') === 'true';
    }
    return direct;
  }

  static getEnvironment () {
    return this.getCommandLineArg('env');
  }

  static isHeadLess () {
    let headless = false;
    if (this.getCommandLineArg('headless') !== '') {
      headless = this.getCommandLineArg('headless') === 'true';
    }
    return headless;
  };

  static getDevice () {
    const platform = this.getBrowser();
    /** setup the default devices for ios and
     * android when user do not send any device arguments
     */
    let device = (platform[0] === 'ios') ? ['iPhone X'] : ['Pixel 3'];
    if (this.getCommandLineArg('device') !== '') {
      device = this.getCommandLineArg('device').split(',');
    }
    return device;
  };

  static getPlatformVersion () {
    let platformVersion = '12.4';
    if (this.getCommandLineArg('platformVersion') !== '') {
      platformVersion = this.getCommandLineArg('platformVersion');
    }
    return platformVersion;
  }
}

module.exports = CommandLineUtility;
