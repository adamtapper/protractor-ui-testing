const CommandLineUtility = require('./command-line-utility');
class MobileUtility {
  static getMobileConfig (platform, deviceList) {
    const deviceConfig = {
      ios: {
        browserName: 'safari',
        platformName: 'iOS',
        platformVersion: CommandLineUtility.getPlatformVersion(),
        orientation: 'PORTRAIT',
        automationName: 'XCUITest',
        app: '',
        noReset: false,
        fullReset: true,
        isHeadless: CommandLineUtility.isHeadLess(),
        newCommandTimeout: 240
      },
      android: {
        browserName: 'Chrome',
        platformName: 'Android',
        automationName: 'UiAutomator2',
        nativeWebScreenshot: true,
        chromedriverArgs: ['--disable-gpu']
      }
    };

    const configList = [];
    deviceList.forEach(device => {
      let stand = {};
      stand = deviceConfig[platform];
      stand['deviceName'] = device;
      const platformConfig = JSON.parse(JSON.stringify(stand));
      configList.push(platformConfig);
    });
    return configList;
  }
}

module.exports = MobileUtility;
