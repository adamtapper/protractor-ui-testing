const fs = require('fs');
const jasmineReporters = require('jasmine-reporters');
const HTMLReport = require('protractor-html-reporter-2');
const DateTimeUtility = require('./date-time-utility');

class ReportUtility {
  constructor () {
    this.reportTitle = 'Protractor Test Execution Report';
    this.reportsDirectory = './reports';
    this.dashboardReportDirectory = this.reportsDirectory + '/' + DateTimeUtility.getReportDateTime(new Date());
  }

  moveDiffImages () {
    const dashboardReportDirectory = this.dashboardReportDirectory;
    browser.getCapabilities().then(function (caps) {
      const browserName = caps.get('browserName');
      const sourceDir = dashboardReportDirectory + '/diff/desktop_' + browserName;
      const destDir = dashboardReportDirectory;
      let files = [];
      if (fs.existsSync(sourceDir)) {
        files = fs.readdirSync(sourceDir);
      }
      for (let i = 0; i < files.length; i++) {
        fs.copyFileSync(sourceDir + '/' + files[i], destDir + '/' + files[i].replace(/_/g, ' '));
      };
    });
  }

  setReportTitle (reportTitle) {
    this.reportTitle = reportTitle;
  }

  getReportTitle () {
    return this.reportTitle;
  }

  getDashboardReportDirectory () {
    return this.dashboardReportDirectory;
  }

  setReportDirectory (reportDirectory) {
    this.reportsDirectory = reportDirectory;
    this.dashboardReportDirectory = this.reportsDirectory + '/' + DateTimeUtility.getReportDateTime(new Date());
  }

  getReportDirectory () {
    return this.reportsDirectory;
  }

  listFiles () {
    fs.readdirSync(this.dashboardReportDirectory + '/actual/**/').forEach(file => {
      console.log(file);
    });
  }

  addReporter (jasmine) {
    const dashboardReportDirectory = this.dashboardReportDirectory;
    jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
      consolidateAll: true,
      savePath: this.reportsDirectory + '/xml',
      filePrefix: 'xmlOutput'
    }));

    if (!fs.existsSync(dashboardReportDirectory)) {
      fs.mkdirSync(dashboardReportDirectory, { recursive: true });
    }

    jasmine.getEnv().addReporter({
      specDone: function (result) {
        if (result.status === 'failed') {
          browser.getCapabilities().then(function (caps) {
            const browserName = caps.get('browserName');
            let files = [];
            if (fs.existsSync(dashboardReportDirectory + '/diff/desktop_' + browserName)) {
              files = fs.readdirSync(dashboardReportDirectory + '/diff/desktop_' + browserName);
            }
            const imageName = browserName + '-' + result.fullName + '.png';
            if (!files.includes(imageName.replace(/ /g, '_'))) {
              browser.takeScreenshot().then(function (png) {
                fs.writeFileSync(dashboardReportDirectory + '/' + browserName + '-' + result.fullName + '.png', png, 'base64', function (err) {
                  if (err) { console.log(err); } else { console.log('Successfully Written to File.'); }
                });
              });
            }
          });
        }
      }
    });
  }

  createReport () {
    const capsPromise = browser.getCapabilities();
    const reportTitle = this.getReportTitle();
    const dashboardReportDirectory = this.dashboardReportDirectory;
    const reportsDirectory = this.reportsDirectory;
    capsPromise.then(function (caps) {
      const browserName = caps.get('browserName');
      const browserVersion = caps.get('version');
      const platform = caps.get('platform');

      const testConfig = {
        reportTitle: reportTitle,
        outputPath: dashboardReportDirectory,
        outputFilename: 'report',
        screenshotPath: '.',
        testBrowser: browserName,
        browserVersion: browserVersion,
        modifiedSuiteName: false,
        screenshotsOnlyOnFailure: true,
        testPlatform: platform
      };
      new HTMLReport().from(reportsDirectory + '/xml/xmlOutput.xml', testConfig);
    });
  }
}

module.exports = ReportUtility;
