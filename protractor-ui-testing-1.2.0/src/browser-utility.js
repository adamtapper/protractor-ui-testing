class BrowserUtility {
  static getBrowserConfig (browserList) {
    const browserConfig = {
      chrome: {
        browserName: 'chrome',
        useAutomationExtension: false,
        chromeOptions: {
          args: ['--remote-debugging-port=9222']
        }
      },
      firefox: {
        browserName: 'firefox'
      },
      safari: {
        browserName: 'safari'
      },
      headlessChrome: {
        browserName: 'chrome',
        useAutomationExtension: false,
        chromeOptions: {
          args: ['--headless', '--disable-gpu', '--remote-debugging-port=9222']
        }
      },
      headlessFirefox: {
        browserName: 'firefox',
        'moz:firefoxOptions': {
          args: ['--headless']
        }
      }
    };
    const configList = [];
    browserList.forEach(browser => configList.push(browserConfig[browser]));
    return configList;
  }
}

module.exports = BrowserUtility;
