function format (integer) {
  if (integer < 10) {
    return `0${integer}`;
  } else {
    return integer;
  }
}

class DateTimeUtility {
  static getDateTime (date) {
    return `${date.getFullYear()}-${format(date.getMonth() + 1)}-${format(date.getDate())}_${format(date.getHours())}:${format(date.getMinutes())}:${format(date.getSeconds())}`;
  }

  static getReportDateTime (date) {
    return `${date.getFullYear()}-${format(date.getMonth() + 1)}-${format(date.getDate())}_${format(date.getHours())}-${format(date.getMinutes())}-${format(date.getSeconds())}`;
  }
}

module.exports = DateTimeUtility;
